package mx.uniceq.holamundo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static String TAG = "MainActivity";
    Button btnIniciar;
    TextView etCrear;
    EditText etUser,etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initElements();
    }//onCreate

    public void initElements(){
        btnIniciar = findViewById(R.id.btnIniciar);
        etCrear = findViewById(R.id.etCrear);
        etUser = findViewById(R.id.etUser);
        etPassword = findViewById(R.id.etPassword);

        btnIniciar.setOnClickListener(this);
        etCrear.setOnClickListener(this);
    } //initElements

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnIniciar:
                obtenerCredencial(etUser.getText().toString(), String.valueOf(etPassword.getText()));
                break;
            case R.id.etCrear:
                Log.e(TAG, "Tap a la etiqueta!!");
                Intent intent = new Intent(this,
                        RegistroActivity.class);
                this.finish();
                startActivity(intent);
                break;
        }//switch
    }//onClick

    public void obtenerCredencial(String email, String password){
        Log.e("USUARIO",email);
        Log.e("PASSWORD",password);
    }//obtenerCredencial

}//MainActivity