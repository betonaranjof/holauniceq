package mx.uniceq.holamundo;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener {

    private static String TAG = "RegistroActivity";
    Button btnRegistroNuevo;
    EditText etNuevoUsuario, etPasswordNuevo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        initElements();
    }//onCreate

    public void initElements(){
        btnRegistroNuevo = findViewById(R.id.btnRegistroNuevo);

        etNuevoUsuario = findViewById(R.id.etNuevoUsuario);
        etPasswordNuevo = findViewById(R.id.etPasswordNuevo);

        btnRegistroNuevo.setOnClickListener(this);

    }//initElements

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnRegistroNuevo:
                nuevoUsuario(etNuevoUsuario.getText().toString(),
                        etPasswordNuevo.getText().toString());
                break;
        }
    }//onClick

    public void nuevoUsuario(String email, String password ){
        Log.d("Email", email);
        Log.d("Password", password);
    }//nuevoUsuario
}//RegistroActivity
